#!/bin/sh

set -e

case "${1}" in
	prereqs)
		exit 0
		;;
esac

. /usr/share/initramfs-tools/hook-functions

# Encode special characters
_urlencode() {
  echo "$@" | awk -v ORS="" '{ gsub(/./,"&\n") ; print }' | while read l;
  do
    case "$l" in
      [-_.~a-zA-Z0-9] ) echo -n ${l} ;;
      "" ) echo -n %20 ;;
      * )  printf '%%%02x' "'$l"
    esac
  done
  echo ""
}

# Unpack a deb file into a specific rootfs
# arguments: package_name dest_dir
dpkg_unpack () {
  PKG_URL="https://mirrors.kernel.org/ubuntu/$(apt-cache show $1 \
    | grep -A 15 "^Maintainer: Ubuntu" | grep "^Filename: " \
    | cut -d ' ' -f 2)"
  PKG_FILE="$(basename $PKG_URL)"
  PKG_PATH="/tmp/$PKG_FILE"
  if [ ! -e $PKG_PATH ]; then
    curl -sLo "$PKG_PATH" "$PKG_URL"
  fi
  dpkg -x $PKG_PATH $2
}

manual_add_modules squashfs
copy_exec /sbin/modprobe $DESTDIR/sbin/

# Add the full busybox version
# and add link to needed modules
STAGINGDIR=`mktemp -d`
dpkg_unpack busybox $STAGINGDIR/
cp -a $STAGINGDIR/bin/busybox $DESTDIR/bin/
cd $DESTDIR/sbin/
ln -s ../bin/busybox ifconfig
ln -s ../bin/busybox route
cd -
cd $DESTDIR/bin/
ln -s ../bin/busybox tftp
ln -s ../bin/busybox dirname
cd -
dpkg_unpack udhcpc $STAGINGDIR
cp -a $STAGINGDIR/etc/udhcpc $DESTDIR/etc/
cp -a $STAGINGDIR/sbin/udhcpc $DESTDIR/sbin/
rm $STAGINGDIR -r

# Patch to support file mount (not only directory)
$DESTDIR/bin/busybox patch -i /etc/initramfs-tools/patches/9990-misc-helpers.sh.patch \
    -p0 $DESTDIR/lib/live/boot/9990-misc-helpers.sh

exit 0
